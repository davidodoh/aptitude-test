const express = require('express');
const mongoose = require('mongoose');
const config = require('./config/database');


const app = express();

// Connecting to MongoDb
mongoose.connect(config.database);

// Checking if connection to db was successful
mongoose.connection.on('connected', () => {
    console.log('Connected to database '+config.database);
});

// on db connection err
mongoose.connection.on('error', (err) => {
    console.log('Database error '+err);
});

// Index Route
app.get('/', (req, res) => {
    res.send('Index Route Found!')
})

const port = 5000;

app.listen(port, () => {
    console.log(`Server started on port ${port}`)
});