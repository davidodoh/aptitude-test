const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestSchema = mongoose.Schema({
    numberOfQuestions : { type: Number },
    questions : [ String ],
    timeAlloted : { type: String },
    markingTechnique : {
        type: String,
        enum: ['Negative', 'Acurate']
    },
    testDate : { type: Date },
    testInstructions : { type: String, uppercase: true, trim: true},
});

module.exports = mongoose.model('Test', TestSchema);