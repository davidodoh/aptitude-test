const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const QuestionSchema = mongoose.Schema({
    questionName : { type: String },
    maxTime : { type: String },
    questionString : {type: String, trim: true},
    choices : [String],
    correctIndex : { Number },
    correctAnswer : {type: String, uppercase: true, trim: true},
    categories : [ String ],
    difficulty : {
        type: String,
        enum: ['Novice', 'Intermediate', 'Expert', 'Professional']
    },
    lastUpdated : {type: Date, default: Date.now}
});

const Question = module.exports = mongoose.model('Question', QuestionSchema);