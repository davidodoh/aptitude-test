const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InterviewQuestionSchema = mongoose.Schema({
    questionName : { type: String },
    maxTime : { type: String },
    questionString : {type: String, trim: true},
    choices : [String],
    correctIndex : { Number },
    correctAnswer : {type: String, uppercase: true, trim: true},
    industry : [ String ],
    jobPosition : [ String ],
    lastUpdated : {type: Date, default: Date.now}
});

module.exports = mongoose.model('InterviewQuestion', InterviewQuestionSchema);