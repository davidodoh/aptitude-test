const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ResponseSchema = mongoose.Schema({
    candidateId : { type: String },
    questionId :  { type: String },
    choiceModeIndex : [ Number ],
    choiceMode : [ String ]
});

module.exports = mongoose.model('Response', ResponseSchema);