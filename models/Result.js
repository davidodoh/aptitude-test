const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ResultSchema = mongoose.Schema({
    candidateId : { type: String },
    testId :  { type: String },
    Mark : { Number },
    Grade : { String } 
});

module.exports = mongoose.model('Result', ResultSchema);